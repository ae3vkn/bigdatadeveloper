val cucumber_jvm       = "info.cukes"      % "cucumber-jvm"         % "1.2.2"
val cucumber_jvm_deps  = "info.cukes"      % "cucumber-jvm-deps"    % "1.0.3"
val cucumber_scala     = "info.cukes"      % "cucumber-scala_2.11"  % "1.2.2"
val cucumber_junit     = "info.cukes"      % "cucumber-junit"       % "1.2.2"
val gherkin            = "info.cukes"      % "gherkin"              % "2.12.2"
val gherkin_deps       = "info.cukes"      % "gherkin-jvm-deps"     % "1.0.3"
val typesafe           = "com.typesafe"    % "config"               % "1.3.0-M3"
val scalatest          = "org.scalatest"   % "scalatest-all_2.11"   % "2.3.0-SNAP2"
val scalareflect       = "org.scala-lang"  % "scala-reflect"        % "2.11.6"
val scalalibrary       = "org.scala-lang"  % "scala-library"        % "2.11.6"
val novocode           = "com.novocode"    % "junit-interface"      % "0.11"

lazy val commonSettings = Seq(
  organization := "works.bdd",
  version := "0.1.0",
  scalaVersion := "2.11.6"
)

seq(cucumberSettings : _*)

cucumberStepsBasePackage := "test"

cucumberHtmlReport := true

cucumberJunitReport := true 

cucumberJsonReport := true

lazy val root = (project in file(".")).
  settings(commonSettings: _*).
  settings(
    name := "BDD Works",
    libraryDependencies += cucumber_jvm,
    libraryDependencies += cucumber_jvm_deps,
    libraryDependencies += cucumber_scala,
    libraryDependencies += cucumber_junit,
    libraryDependencies += gherkin,
    libraryDependencies += gherkin_deps,
    libraryDependencies += typesafe,
    libraryDependencies += scalatest,
    libraryDependencies += scalareflect,
    libraryDependencies += scalalibrary,
    libraryDependencies += "junit" % "junit" % "4.12" % "test",
    libraryDependencies += novocode
)

