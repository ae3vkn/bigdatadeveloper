FROM centos:6
MAINTAINER "Big Data Partnership - Mark Olliver" <mark.olliver@bigdatapartnership.com>
ENV container docker
RUN rpm -Uvh http://download.fedoraproject.org/pub/epel/6/i386/epel-release-6-8.noarch.rpm
WORKDIR /etc/yum.repos.d
RUN curl https://bintray.com/sbt/rpm/rpm -o bintray-sbt-rpm.repo
RUN yum -y update
RUN yum -y install initscripts sudo redhat-lsb-core postfix \
    rsyslog openssh openssh-server openssh-clients \
    curl wget nc iotop tar bzip2 gwak gzip \
    java-1.7.0-openjdk-headless java-1.7.0-openjdk-devel \
    make autoconf automake htop screen lsof \
    emacs emacs-git emacs-global emacs-mercurial \
    vim-enhanced vim-X11 nano \
    eclipse-platform eclipse-egit eclipse-linuxtools eclipse-jdt \
    gcc pkgconfig cmake gtk2-devel tk-devel db4-devel \
    scala scala-swing scalapack-common \
    subversion git git-gui git-hg mercurial hgview-curses \
    firefox \
    python-httplib2 zlib-devel bzip2-devel openssl-devel sqlite-devel \
    freetype-devel libpng-devel lapack lapack-devel blas blas-devel numpy \
    scipy python-pandas sympy python-nose \
    xorg-x11-server-Xvfb xorg-x11-fonts-Type1 xorg-x11-fonts-misc \
    dejavu-lgc-sans-fonts xorg-x11-fonts-75dpi xorg-x11-fonts-100dpi \
    xorg-x11-fonts-ISO8859-1-75dpi.noarch xorg-x11-fonts-ISO8859-1-100dpi.noarch \
    xterm xorg-x11-utils gnome-panel gnome-terminal gnome-applets nautils \
    xorg-x11-xdm vlgothic-fonts vlgothic-p-fonts ipa-gothic-fonts ipa-mincho-fonts \
    ipa-pgothic-fonts ipa-pmincho-fonts nautilus-open-termeinal system-gnome-theme \
    system-icon-theme tigervnc-server tigervnc ncurses-devel readline-devel libpcap-devel \
    xz-devel sbt docker-io
RUN yum -y groupinstall "development tools"
RUN yum clean all
RUN service sshd start; service sshd stop
RUN sed -i '$ a\export PATH=/usr/local/bin\:\$PATH' /etc/profile
RUN sed -i '$ a\/usr/local/lib' /etc/ld.so.conf && \
    /sbin/ldconfig
ADD sudoers /etc/sudoers
RUN chown root:root /etc/sudoers
ADD start.sh /bin/start.sh
RUN sed -i 's$:0 local /usr/bin/X :0$:0 local /usr/bin/Xvnc :0 -geometry 1600x1200 -depth 32 -SecurityTypes None -desktop CentOS6 DisconnectClients=0$' /etc/X11/xdm/Xservers
RUN sed -i "s/DisplayManager.requestPort:\t0/DisplayManager.requestPort:   177/" /etc/X11/xdm/xdm-config
RUN echo '*' >> /etc/X11/xdm/Xaccess
RUN mkdir /usr/local/apache-maven
WORKDIR /usr/local/apache-maven
RUN wget http://apache.mirror.anlx.net/maven/maven-3/3.3.3/binaries/apache-maven-3.3.3-bin.tar.gz
RUN tar zxf apache-maven-3.3.3-bin.tar.gz && \
    rm apache-maven-3.3.3-bin.tar.gz
RUN sed -i '$ a\export PATH=/usr/local/apache-maven/apache-maven-3.3.3/bin\:\$PATH' /etc/profile
RUN mkdir /usr/local/scala
WORKDIR /usr/local/scala
RUN wget http://downloads.typesafe.com/scala/2.11.6/scala-2.11.6.tgz
RUN tar zxf scala-2.11.6.tgz && \
    rm scala-2.11.6.tgz
RUN wget http://downloads.typesafe.com/scala/2.10.3/scala-2.10.3.tgz
RUN tar zxf scala-2.10.3.tgz && \
    rm scala-2.10.3.tgz
RUN sed -i '$ a\export PATH=/usr/local/scala/scala-2.10.3/bin\:\$PATH' /etc/profile
RUN chkconfig --level 345 sshd on 
ADD sshd_config /etc/ssh/sshd_config
RUN chmod 600 /etc/ssh/sshd_config
ADD start.sh /bin/start.sh
RUN echo "NETWORKING=yes" > /etc/sysconfig/network
RUN sed -i '/pam_loginuid\.so/s/required/optional/' /etc/pam.d/sshd
RUN echo " " > /sbin/start_udev
RUN useradd -m -s /bin/bash -G wheel dev
RUN mkdir /home/dev/.ssh
ADD authorized_keys /home/dev/.ssh/authorized_keys
RUN chmod 700 /home/dev/.ssh; chmod 600 /home/dev/.ssh/authorized_keys; chown -R dev:dev /home/dev/.ssh
RUN echo "dev:dev" | chpasswd
WORKDIR /usr/src
RUN curl -LO http://python.org/ftp/python/2.7.9/Python-2.7.9.tgz && \
    tar xzf Python-2.7.9.tgz
WORKDIR /usr/src/Python-2.7.9
RUN ./configure --prefix=/usr/local --enable-shared --enable-unicode=ucs4 LDFLAGS="-Wl,-rpath /usr/local/lib" && \
    make && make altinstall && \
    curl https://bootstrap.pypa.io/ez_setup.py -o - | python2.7 && \
    easy_install-2.7 pip && \
    make clean
WORKDIR /usr/src
RUN rm Python-2.7.9.tgz
RUN pip2.7 install virtualenv
USER dev
WORKDIR /home/dev
RUN virtualenv python2.7_env
WORKDIR /home/dev/python2.7_env
RUN sed -i '$ a\source /home/dev/python2.7_env/bin/activate' /home/dev/.bashrc
RUN bin/pip install -Iv "ipython[notebook]"
RUN bin/pip install -Iv matplotlib==1.4.3
RUN bin/pip install -Iv decorator==3.4.2
RUN bin/pip install -Iv logging==0.4.9.6
RUN bin/pip install -Iv mock==1.0.1
RUN bin/pip install -Iv networkx==1.9.1
RUN bin/pip install -Iv nltk==3.0.2
RUN bin/pip install -Iv nose==1.3.6
RUN bin/pip install -Iv numpy==1.9.2
RUN bin/pip install -Iv pandas==0.16.0
RUN bin/pip install -Iv Pillow==2.8.1
RUN bin/pip install -Iv pyparsing==2.0.3
RUN bin/pip install -Iv python-dateutil==2.4.2
RUN bin/pip install -Iv pytz==2015.2
RUN bin/pip install -Iv scipy==0.15.1
RUN bin/pip install -Iv scikit-image==0.11.3
RUN bin/pip install -Iv scikit-learn==0.16.0
RUN bin/pip install -Iv six==1.9.0
RUN bin/pip install -Iv ecdsa
RUN bin/pip install -Iv paramiko
RUN bin/pip install -Iv pycrypto
RUN bin/pip install -Iv fabric
RUN bin/pip install -Iv ansible
WORKDIR /home/dev
RUN mkdir tools data bin cucumber
WORKDIR /home/dev/tools
RUN wget https://github.com/Itseez/opencv/archive/3.0.0-beta.zip
RUN unzip 3.0.0-beta.zip
RUN mkdir opencv-3.0.0-beta/build
WORKDIR /home/dev/tools/opencv-3.0.0-beta/build
RUN cmake ../ -DCMAKE_BUILD_TYPE=RELEASE -DCMAKE_INSTALL_PREFIX=/home/dev/python2.7_env -DBUILD_EXAMPLES=OFF -DBUILD_NEW_PYTHON_SUPPORT=ON  \ 
 -DPYTHON_EXECUTABLE=/home/dev/python2.7_env/bin/python2.7 -DPYTHON_INCLUDE_DIR=/home/dev/python2.7_env/include/python2.7/ \
 -DPYTHON_LIBRARY=/usr/local/lib/libpython2.7.so.1.0 -DPYTHON_NUMPY_INCLUDE_DIR=/home/dev/python2.7_env/lib/python2.7/site-packages/numpy/core/include/ \
 -DPYTHON_PACKAGES_PATH=/home/dev/python2.7_env/lib/python2.7/site-packages/ -DBUILD_PYTHON_SUPPORT=ON && \
 make -j2 && make install && make clean
WORKDIR /home/dev/tools
RUN wget http://download.jetbrains.com/idea/ideaIC-14.1.tar.gz
RUN wget http://c758482.r82.cf2.rackcdn.com/sublime_text_3_build_3083_x64.tar.bz2
RUN wget https://lastpass.com/lplinux.tar.bz2

EXPOSE 22
EXPOSE 5900

VOLUME ["/home/dev/data"]

USER root
WORKDIR /home/dev
CMD ["/bin/start.sh"]


################################################################################
#   Copyright 2015 Big Data Partnership Ltd                                    #
#                                                                              #
#   Licensed under the Apache License, Version 2.0 (the "License");            #
#   you may not use this file except in compliance with the License.           #
#   You may obtain a copy of the License at                                    #
#                                                                              #
#       http://www.apache.org/licenses/LICENSE-2.0                             #
#                                                                              #
#   Unless required by applicable law or agreed to in writing, software        #
#   distributed under the License is distributed on an "AS IS" BASIS,          #
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   #
#   See the License for the specific language governing permissions and        #
#   limitations under the License.                                             #
################################################################################

