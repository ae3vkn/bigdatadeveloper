#!/bin/bash

USER=${USER:-admin}
PASSWORD=${PASSWORD:-admin}
LANG=${LANG:-en_US.UTF-8}
TIMEZONE=${TIMEZONE:-Etc/UTC}

echo "LANG=\"${LANG}\"" > /etc/sysconfig/i18n 
echo "ZONE=\"${TIMEZONE}\"" >  /etc/sysconfig/clock
echo "UTC=\"True\""         >> /etc/sysconfig/clock
cp -p "/usr/share/zoneinfo/${TIMEZONE}" /etc/localtime 

echo "root:${PASSWORD}" |chpasswd

if [ ! -d /home/${USER} ] ; then
  useradd -m -k /etc/skel -s /bin/bash  ${USER}
  echo "${USER}:${PASSWORD}" |chpasswd
fi

/usr/bin/xdm
# Start the ssh service
/usr/sbin/sshd -D


################################################################################
#   Copyright 2015 Big Data Partnership Ltd                                    #
#                                                                              #
#   Licensed under the Apache License, Version 2.0 (the "License");            #
#   you may not use this file except in compliance with the License.           #
#   You may obtain a copy of the License at                                    #
#                                                                              #
#       http://www.apache.org/licenses/LICENSE-2.0                             #
#                                                                              #
#   Unless required by applicable law or agreed to in writing, software        #
#   distributed under the License is distributed on an "AS IS" BASIS,          #
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   #
#   See the License for the specific language governing permissions and        #
#   limitations under the License.                                             #
################################################################################

