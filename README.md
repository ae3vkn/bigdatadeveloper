# Big Data Partnership - Developer GUI Docker Workstation

This Docker container is a full development environment delivered via VNC over SSH. You can logon to it using the user 'dev' with the public vagrant ssh key.

To start the Docker container do:

    docker run -d -h NAME --name NAME -p 2222:22 -p 5901:5900 bdpzone/bigdatadeveloper

If the container has already been run but is stopped you can do:

    docker start NAME

(if you want to mount a data volume to the image you can use the '-v localpath:remotepath' flags to the docker run command


If you started the server on a remote server then to connect you will need to use SSH tunnel forwarding using the following commands:

    ssh -L 2222:localhost:2222 -L 5901:localhost:5901 user@remote_server


Now you can use any vnc or ssh client to access the docker container data:

    ssh -p 2222 dev@localhost
    vinagre localhost:1


This container does work on Windows via the use of Boot2docker, I would also then recommend the use of RealVNC for the VNC client. When connecting to the container using windows you need to use the IP 192.168.59.103 instead of localhost.


SSH connections to this workstation by default use the user "dev" with the public vagrant ssh key, it is advised you change this upon first connection.


Vagrant SSH Key - https://raw.githubusercontent.com/mitchellh/vagrant/master/keys/vagrant



To resize the desktop to a useable size run the following commands from within a terminal session on the server:

    xrandr -q

Which lists the supported X sizes, then:

    xrandr --output default --mode XxX

    
## DockerFile
The original dockerfile and source for this repository can be found on BitBucket here:

    https://bitbucket.org/bigdatapartnership/bigdatadeveloper.git


## Windows
To enable you to run docker on windows and mount a local windows volume along with docker control from inside docker do the following steps:

1. Create a directory inside your windows users home directory e.g c:/Users/DevOps/data
2. Inside the directory create a file called 'env_docker' with the following contents:

```
export DOCKER_HOST=tcp://192.168.59.103:2376
export DOCKER_TLS_VERIFY=1
```

3. Run docker with the following parameters, changing the value DevOps for your user/path and HOSTNAME for the name you want the container to have:

 
```
    docker run -d -h HOSTNAME --name HOSTNAME -p 2222:22 -p 5901:5900 --privileged -v //c/Users/DevOps/data:/home/dev/data -v //c/Users/DevOps/.boot2docker/certs/boot2docker_vm:/home/dev/.docker bdpzone/bigdatadeveloper
```

```
################################################################################
#   Copyright 2015 Big Data Partnership Ltd                                    #
#                                                                              #
#   Licensed under the Apache License, Version 2.0 (the "License");            #
#   you may not use this file except in compliance with the License.           #
#   You may obtain a copy of the License at                                    #
#                                                                              #
#       http://www.apache.org/licenses/LICENSE-2.0                             #
#                                                                              #
#   Unless required by applicable law or agreed to in writing, software        #
#   distributed under the License is distributed on an "AS IS" BASIS,          #
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   #
#   See the License for the specific language governing permissions and        #
#   limitations under the License.                                             #
################################################################################
```
